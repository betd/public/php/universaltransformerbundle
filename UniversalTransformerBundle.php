<?php

declare(strict_types=1);


namespace BusinessDecision\Bundle\UniversalTransformer;

use BusinessDecision\Component\Transformer\DependencyInjection\TransformerPass;
use BusinessDecision\Component\Transformer\TransformerInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;


class UniversalTransformerBundle  extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->registerForAutoconfiguration(TransformerInterface::class)
            ->addTag(TransformerPass::TRANSFORMER_TAG)
        ;


        $container->addCompilerPass(new TransformerPass());
    }
}
